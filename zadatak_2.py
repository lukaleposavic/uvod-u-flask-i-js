
import pymysql.cursors
from flask import Flask, jsonify
from flask_restful import Resource, Api, reqparse


app = Flask(__name__)
api = Api(app)
data = {}
employees_data = []
office_data = []

connection = pymysql.connect(host='localhost',
                             user='root',
                             password='password',
                             database='classicmodels',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor
                             ) 
cursor = connection.cursor()







class Employees(Resource):
    def get(self):
        query = '''select concat(e1.firstName, " " ,e1.lastName) as "full_name",e1.email,e1.officeCode,e1.jobTitle,concat(e2.firstName," ",e2.lastName)
         as reports_to from employees e1, employees e2 where e1.reportsTo = e2.employeeNumber '''
        cursor.execute(query)
        employees_data = cursor.fetchall()
        return jsonify(employees_data)

    def post(self):
        try:
            parser = reqparse.RequestParser()
            parser.add_argument("employeeNumber", required = True, type = int)
            parser.add_argument("lastName", required = True, type = str)
            parser.add_argument("firstName", required = True, type = str)
            parser.add_argument("extension", required = True, type = str)
            parser.add_argument("email", required = True, type = str)
            parser.add_argument("officeCode", required = True, type = str)
            parser.add_argument("reportsTo", required = True, type = int)
            parser.add_argument("jobTitle", required = True, type = str)
            args = parser.parse_args()
            cursor.execute(f"""insert into employees(employeeNumber,lastName,firstName,extension,email,officeCode,reportsTo,jobTitle) 
            values ('{args['employeeNumber']}','{args['lastName']}','{args['firstName']}','{args['extension']}','{args['email']}','{args['officeCode']}',
            {args['reportsTo']},'{args['jobTitle']}');""")
            connection.commit()
        
        except Exception as e:
            return jsonify({'error':'invalid input'})

    def delete(self):
        try:
            parser = reqparse.RequestParser()
            parser.add_argument("employeeNumber", required = True, type = int)
            args = parser.parse_args()
            cursor.execute(f"delete from employees where employeeNumber = {args['employeeNumber']};")
            connection.commit()
        
        except Exception as e:
            return jsonify({'error':'invalid input'})

    def put(self):
        try:
            parser = reqparse.RequestParser()
            parser.add_argument("employeeNumber", required = True, type = int)
            parser.add_argument("lastName", required = True, type = str)
            parser.add_argument("firstName", required = True, type = str)
            parser.add_argument("extension", required = True, type = str)
            parser.add_argument("email", required = True, type = str)
            parser.add_argument("officeCode", required = True, type = str)
            parser.add_argument("reportsTo", required = True, type = int)
            parser.add_argument("jobTitle", required = True, type = str)
            args = parser.parse_args()
            cursor.execute(f"""update employees set lastName = '{args['lastName']}',firstName = '{args['firstName']}',extension = '{args['extension']}',
            email = '{args['email']}', officeCode = '{args['officeCode']}', reportsTo = {args['reportsTo']}, jobTitle = '{args['jobTitle']}'
            where employeeNumber = {args['employeeNumber']} """)
            connection.commit()

        except Exception as e:
            return jsonify({'error':'invalid input'})




class Office_code(Resource):
    def get(self):
        query = '''select o.country,o.city,o.postalCode,concat(o.addressLine1," ",addressLine2) as "address_line",concat(e.firstName, " " ,e.lastName) 
        as "employee_name" from offices o,employees e where e.officeCode = o.officeCode'''
        cursor.execute(query)
        office_data = cursor.fetchall()
        return jsonify(office_data)




api.add_resource(Employees, '/employees')  # this is our entry point for Employees
api.add_resource(Office_code,'/office/office_code')
if __name__ == '__main__':
    app.run(debug = True)
